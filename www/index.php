<?php
//	ini_set('display_errors', 1);
	include __DIR__ . '/../vendor/Zappi.php';
	include __DIR__ . '/../vendor/zappi/Application.php';

	include __DIR__."/../model/User.php";
	include __DIR__."/../model/Channel.php";
	include __DIR__."/../vendor/Controller.php";
	include __DIR__."/../vendor/UrlManager.php";

(new \zappi\Application())->run();
	$query = $_GET;
	if(isset($_GET['route'])){
		$route = $_GET['route'];
		unset($_GET['route']);
	} else {
        $route = 'main/index';
	}

	$controller = ucfirst(split('/', $route)[0].'Controller');
	$action = 'action'.ucfirst(split('/', $route)[1]);
	include __DIR__ . "/../controller/" .$controller.".php";
	$cClass = new $controller();
	if(method_exists($cClass, $action)){
		http_response_code(200);
		if(sizeof($_GET) > 0){
            $method = new \ReflectionMethod($cClass, $action);
			$args = [];
			$actionParams = [];
            foreach ($method->getParameters() as $parameter){
                if(isset($_GET[$parameter->getName()])){
                    $name = $parameter->getName();
//                    $$name = $_GET[$name];
                    $args[] = $actionParams[$name] = $_GET[$name];
                }
            }
            $result = call_user_func_array([$cClass, $action], $args);
//			$cClass->$action($args);
		} else {
			$cClass->$action();
		}
	} else {
		http_response_code(404);
		echo "404 not found";
	}



?>
