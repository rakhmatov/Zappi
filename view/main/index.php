<?php
/**
 * Created by PhpStorm.
 * User: izzat
 * Date: 28.05.17
 * Time: 11:26
 */
?>
<div class="content">
    <div class="section section__theme_shadow">
        <div class="container">
            <div class="content__tv channels-grid">
                <a href="#" class="channels-grid__link channels-grid__cover channels-grid__cover_icon_tv">
                    <svg class="channels-grid__icon">
                        <use xlink:href="#tv"></use>
                    </svg>
                    <p class="channels-grid__title">AllChannels <span class="channels-grid__count"><?= sizeof($data) ?></span></p>
                </a>
                <?php
                $index = 0;
                foreach($data as $channel){
                    if($index == 17) break;
                    $index++;?>
                    <a href="#" title="<?= $channel->name ?>" class="channels-grid__link">
                        <img src="<?='http://media.mediabay.uz/www_data/'.str_replace(".png", ".98x60.png", $channel->logo)?>" class="channels-grid__img"/>
                        <?php if($channel->commercialChannel){ ?>
                            <svg class="icon-payment payment_icon-s">
                                <use xlink:href='#payment'></use>
                            </svg>
                        <?php } ?>
                    </a>

                    <?php
                } ?>
            </div>
        </div>
    </div>
</div>
