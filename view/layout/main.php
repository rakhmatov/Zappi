<?php
/**
 * Created by PhpStorm.
 * User: izzat
 * Date: 28.05.17
 * Time: 11:28
 */
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="apple-itunes-app" content="app-id=823575865">
    <META NAME="ROBOTS" CONTENT="NOARCHIVE">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/favicon/apple-touch-icon-114x114.png">
    <!-- Chrome, Firefox OS and Opera-->
    <meta name="theme-color" content="#3f4691">
    <link rel="stylesheet" href="/css/style.min.css" />
    <script type="text/javascript" src="/js/scripts.min.js"></script>
    <!-- Windows Phone-->
    <meta name="msapplication-navbutton-color" content="#3f4691">
    <!-- iOS Safari-->
    <meta name="apple-mobile-web-app-status-bar-style" content="#3f4691">
    <title><?= "titleeee" ?></title>
</head>
<body>
<div class="mobile-header">
    <div class="mobile-header__logo">
        <p>Mediabay</p>
    </div>
    <button class="hamburger hamburger--squeeze" type="button" aria-label="Menu"><span class="hamburger-box"></span><span class="hamburger-inner"></span></button>
</div>

<div class="mobile-header__wrap">
    <form class="search header__search" action="<?='alo'?>" method="post">
        <input class="search__input" type="text" placeholder="Телеканалы, фильмы, сериалы" autocomplete="off" name="Media[title]">
        <button class="search__btn">
            <svg class="search__icon_search">
                <use xlink:href="#search"></use>
            </svg>
        </button>
        <!--Container for live search-->
        <ul class="search__result">
            <li><a class="search__link" href="#">Результат</a></li>
            <li><a class="search__link" href="#">Результат</a></li>
            <li><a class="search__link" href="#">Результат</a></li>
            <li><a class="search__link" href="#">Результат</a></li>
            <li><a class="search__link" href="#">Результат</a></li>
            <li><a class="search__link" href="#">Результат</a></li>
            <li><a class="search__link" href="#">Результат</a></li>
        </ul>
    </form>

    <hr class="mobile-header__hr">
    <div class="user-bar header__user-bar">
        <a class="user-bar__btn user-bar__btn-position user-bar__btn_theme_brand" href="<?=('site/login')?>">Вход</a>
        <a class="user-bar__btn user-bar__btn-position user-bar__btn_theme_default" href="<?=('users/signup')?>">Регистрация</a>
    </div>
    <hr class="mobile-header__hr">
    <nav>
        <div class="container">
            <nav class="nav">
                <ul>
                    <li class="nav__item nav__item_only-mobile"><a href="<?php //Yii::$app->getHomeUrl() ?>" class="nav__link" tabindex="1">Главная</a></li>
                    <?php
//                    $lTypes = Type::listTypes(3);
//                    //                                                    var_dump($lTypes);
//                    for($i = 0; $i < sizeof($lTypes); $i++){
//                        if(sizeof($lTypes[$i]) <= 2){
//                            if(isset($lTypes[$i]["url"]["route"])){
//                                echo '<li class="nav__item">'.Html::a($lTypes[$i]["label"], \Yii::$app->homeUrl.$lTypes[$i]["url"]["route"], ['tabindex'=>$i+1, 'class'=>'nav__link']).'</li>';
//                            } else {
//                                echo '<li class="nav__item">'.Html::a($lTypes[$i]["label"], $lTypes[$i]["url"]["link"], ['tabindex'=>$i+1, 'class'=>'nav__link']).'</li>';
//                            }
//                        } else {
//                            if(isset($lTypes[$i]["url"]["route"])){
//                                echo '<li class="nav__item nav__item_sub">'.Html::a($lTypes[$i]["label"].' <svg class="nav__sub-icon"><use xlink:href="#arrow-down-think"></use></svg>', "#", ['tabindex'=>$i+1, 'class'=>'nav__link']);
//                                echo '<ul class="nav__sub">';
//                                for($jk=0; $jk<sizeof($lTypes[$i])-2; $jk++){
//                                    echo '<li class="nav__sub-nav-item">'.Html::a($lTypes[$i][$jk]["label"], \Yii::$app->homeUrl.$lTypes[$i][$jk]["url"]["route"], ['class'=>'nav__sub-nav-link']).'</li>';
//                                }
//                                echo '</ul>';
//                                echo '</li>';
//                            } else {
//                                echo '<li class="nav__item">'.Html::a($lTypes[$i]["label"], $lTypes[$i]["url"]["link"], ['tabindex'=>$i+1, 'class'=>'nav__link']);
//                                echo '<ul class="nav__sub">';
//                                for($jk=0; $jk<sizeof($lTypes[$i])-2; $jk++){
//                                    echo '<li class="nav__sub-nav-item">'.Html::a($lTypes[$i][$jk]["label"], \Yii::$app->homeUrl.$lTypes[$i][$jk]["url"]["route"], ['class'=>'nav__sub-nav-link']).'</li>';
//                                }
//                                echo '</ul>';
//                                echo '</li>';
//                            }
//                        }
//                    } ?>
                </ul>
            </nav>
        </div>
    </nav>
</div>
<div class="wrapper">
    <header class="header">
        <div class="container header__row">
            <div class="logo"><a href="/"><img class="logo__img" src="/img/logotypeSmall.png" style="max-height:60px;" alt="Logotype"></a></div>
            <form class="search header__search" action="#" method="post">
                <input class="search__input" type="text" placeholder="Телеканалы, фильмы, сериалы" autocomplete="off" name="Media[title]">
                <button class="search__btn">
                    <svg class="search__icon_search">
                        <use xlink:href="#search"></use>
                    </svg>
                </button>
                <!--Container for live search-->
                <ul class="search__result">
                    <li><a class="search__link" href="#">Результат</a></li>
                    <li><a class="search__link" href="#">Результат</a></li>
                    <li><a class="search__link" href="#">Результат</a></li>
                    <li><a class="search__link" href="#">Результат</a></li>
                    <li><a class="search__link" href="#">Результат</a></li>
                    <li><a class="search__link" href="#">Результат</a></li>
                    <li><a class="search__link" href="#">Результат</a></li>
                </ul>

            </form>
        </div>
    </header>
    <div class="content">
        <?php include __DIR__.'/../'.$content_view; ?>
    </div>
    <div class="pre-footer">
        <ul class="container">
            <li class="pre-footer__item"><a class="pre-footer__link pre-footer__link_android" href="https://play.google.com/store/apps/details?id=com.mediabay&amp;hl=ru" target="_blank" rel="nofollow"></a></li>
            <li class="pre-footer__item"><a class="pre-footer__link pre-footer__link_ios" href="https://itunes.apple.com/ru/app/mediabay-tv/id823575865?mt=8" target="_blank" rel="nofollow"></a></li>
            <li class="pre-footer__item"><a class="pre-footer__link pre-footer__link_wp" href="https://www.microsoft.com/en-us/store/p/mediabay-tv/9nbvc61gz3ch" target="_blank" rel="nofollow"></a></li>
            <li class="pre-footer__item"><a class="pre-footer__link pre-footer__link_wpc" href="https://www.microsoft.com/en-us/store/p/mediabay-tv/9nbvc61gz3ch" target="_blank" rel="nofollow"></a></li>
        </ul>
    </div>

    <footer class="footer">
        <div class="container">
            <div class="footer__row">
                <div class="footer__site-info site-info">
                    <p class="site-info__head">2017 © Mediabay.uz</p>
                </div>
                <div class="footer__contacts contacts">
                    <p class="contacts__head">Круглосуточный Call-центр</p><a class="contacts__item" href="tel:+998712310000">+(998 71) 231-00-00</a>
                    <p class="contacts__head">Связаться через:</p>
                    <ul class="social-nav">
                        <li class="social-nav__item"><a class="social-nav__link social-nav__link_telegram" href="http://t.me/mediabay" title="telegram">
                                <svg class="icon_telegram">
                                    <use xlink:href="#telegram-logo"></use>
                                </svg></a></li>
                        <li class="social-nav__item"><a class="social-nav__link social-nav__link_skype" href="#" title="skype">
                                <svg class="icon_skype">
                                    <use xlink:href="#skype-logo"></use>
                                </svg></a></li>
                    </ul>
                </div>
                <ul class="social-nav footer__social-nav">
                    <div class="fb-page" data-href="http://www.facebook.com/pages/Mediabay/534474619925674" data-width="320" data-height="170" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="false"><blockquote cite="http://www.facebook.com/pages/Mediabay/534474619925674" class="fb-xfbml-parse-ignore"><a href="http://www.facebook.com/pages/Mediabay/534474619925674">Mediabay</a></blockquote></div>
                    <li class="social-nav__item"><a class="social-nav__link social-nav__link_facebook" href="http://www.facebook.com/pages/Mediabay/534474619925674" title="facebook">
                            <svg class="icon_facebook">
                                <use xlink:href="#facebook"></use>
                            </svg></a></li>
                    <li class="social-nav__item"><a class="social-nav__link social-nav__link_odnoklassniki" href="http://www.odnoklassniki.ru/profile/538542094045" title="odnoklassniki">
                            <svg class="icon_odnoklassniki">
                                <use xlink:href="#odnoklassniki"></use>
                            </svg></a></li>
                    <li class="social-nav__item"><a class="social-nav__link social-nav__link_twitter" href="https://twitter.com/MediaBayTV" title="twitter">
                            <svg class="icon_twitter">
                                <use xlink:href="#twitter"></use>
                            </svg></a></li>
                    <li class="social-nav__item"><a class="social-nav__link social-nav__link_youtube" href="https://www.youtube.com/channel/UCrdOXSZcVaBLk2t8UARhdVg" title="youtube">
                            <svg class="icon_youtube">
                                <use xlink:href="#youtube"></use>
                            </svg></a></li>
                    <li class="social-nav__item"><a class="social-nav__link social-nav__link_vk" href="http://vk.com/id176134313" title="vkontakte">
                            <svg class="icon_vk">
                                <use xlink:href="#vk"></use>
                            </svg></a></li>
                    <li class="social-nav__item"><a class="social-nav__link social-nav__link_instagram" href="https://www.instagram.com/mediabay_tv" title="instagram">
                            <svg class="icon_instagram">
                                <use xlink:href="#instagram"></use>
                            </svg></a></li>
                </ul>
            </div>

            <noscript><div><img src="//mc.yandex.ru/watch/16905460" style="position:absolute; left:-9999px;" alt="" /></div></noscript>

        </div>
    </footer>
</div>
</body>
</html>
