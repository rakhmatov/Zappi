<?php
/**
 * Created by PhpStorm.
 * User: izzat
 * Date: 29.05.17
 * Time: 13:49
 */

namespace zappi;


class UrlManager
{

    /**
     * UrlManager constructor.
     */
    public function __construct()
    {

    }

    /**
     * @param $params
     * @return string
     */
    public function createUrl($params){
        if(is_array($params)){
            $url = "?route=";
            $route = $params[0];
            $url.=$route;
            unset($params[0]);
            foreach ($params as $key=>$param){
                $url.="&".$key."=".$param;
            }
            return $url;
        }
    }
}