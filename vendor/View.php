<?php
/**
 * Created by PhpStorm.
 * User: izzat
 * Date: 28.05.17
 * Time: 11:35
 */

namespace app\vendor;

class View {
    function generate($content_view, $layout_view, $data = null){
        include __DIR__ . '/../view/' . $layout_view;
    }

}