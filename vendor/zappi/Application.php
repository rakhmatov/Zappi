<?php
/**
 * Created by PhpStorm.
 * User: izzat
 * Date: 28.05.17
 * Time: 12:53
 */
namespace zappi;


use PDO;

class Application {
    public $db;
    public $UrlManager;

    public function run() {
        $dbconfig = include __DIR__."/../../config/db.php";
        $this->db = new PDO("mysql:host={$dbconfig['host']};dbname={$dbconfig['database']};", "{$dbconfig['user']}", "{$dbconfig['password']}");

        $this->UrlManager = new UrlManager();
        \Zappi::$app = $this;
    }

    /**
     * @return mixed
     */
    public function getDb()
    {
        return $this->db;
    }
}